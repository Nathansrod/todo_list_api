import json
from xmlrpc.client import boolean
from flask import Flask, jsonify, request, Response
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/nathan/PDM/extras/TodoListAPI/todolistapp.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class Task(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    content = db.Column(db.String(128), nullable = False)
    isUrgent = db.Column(db.Boolean, nullable = False)
    isDone = db.Column(db.Boolean, nullable = False)

    def toDict(self):
        return{
            'id': self.id,
            'content': self.content,
            'isUrgent': self.isUrgent,
            'isDone': self.isDone
        }

    @staticmethod
    def new(task_dict: dict) -> 'Task':
        task = Task(
            content=task_dict['content'],
            isUrgent=task_dict['isUrgent'],
            isDone=task_dict['isDone']
        )
        db.session.add(task)
        db.session.commit()
        return task

@app.route('/tasks', methods=['GET'])
def get_tasks():
    ret = []
    for task in Task.query.all():
        ret.append(task.toDict())
    return jsonify(ret)

@app.route('/tasks/new', methods=['POST'])
def new_task():
    try:
        task_dict = request.get_json()
        Task.new(task_dict)
        return Response(status=201)
    except KeyError:
        return Response(status=400)
    
@app.route('/tasks/del/<id>', methods=['DELETE'])
def delete_task(id):
    task = Task.query.get(id)
    db.session.delete(task)
    db.session.commit()
    return Response(status=200)

@app.route('/tasks/done/<bool>/<id>', methods=['PUT'])
def update_task(bool, id):
    task = Task.query.get(id)
    if bool == "true":
        task.isDone = True
    else:
        task.isDone = False
    db.session.commit()
    return Response(status=200)