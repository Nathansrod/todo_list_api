# todo_list_API

Pequeno exemplo de API em Flask para o app TodoList, da disciplina de PDM do curso de Engenharia de Computação do IFTM.

## Instalação da API
A API foi criada utilizando um virtual enviroment, para evitar mistura de dependências do python. Execute o .venv da API e realize um pip install, se necessário. As dependências do pyhton estão no arquivo requirements.txt.

Com o .venv em execução e as dependências baixadas, execute flask run --host=0.0.0.0 - esse comando irá rodar a API permitindo que ela seja visível por hosts externos.

No aplicativo, o IP da rota está como 10.0.2.2:5000, que é usado para que o emulador acesse o localhost. Caso você esteja em uma configuração diferente, pode ser necessário configurar o emulador e alterar esse IP, que está no arquivo TasksRequest.

## License
MIT License

Copyright (c) 2022 Nathan Silva Rodrigues

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


